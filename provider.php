<?php

defined('ABSPATH') || die('No script kiddies please!');

use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Provider\GenericResourceOwner;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;

class MITAlumniAssociation extends GenericProvider
{
    /**
     * URL where the user's class year can be retrieved.
     *
     * @var string
     */
    protected $urlClassYear;

    /**
     * URL where the user's club memberships can be retrieved.
     *
     * @var string
     */
    protected $urlClubMemberships;

    /**
     * Indicates if we should use production servers.
     *
     * @var bool
     */
    protected $production;

    private const DEV_API_HOST = 'https://alumapi-dev.mit.edu';
    private const DEV_SSO_HOST = 'https://alumsso-dev.mit.edu';

    private const PRODUCTION_API_HOST = 'https://alumapi.mit.edu';
    private const PRODUCTION_SSO_HOST = 'https://alumsso.mit.edu';


    public function __construct(array $options = array(), array $collaborators = array())
    {
        $use_production = $options['production'];

        $api_host = $use_production ? self::PRODUCTION_API_HOST : self::DEV_API_HOST;
        $sso_host = $use_production ? self::PRODUCTION_SSO_HOST : self::DEV_SSO_HOST;

        $default_options = array(
            'urlAuthorize'            => "{$sso_host}/oauth/authorize",
            'urlAccessToken'          => "{$sso_host}/oauth/token",
            'urlResourceOwnerDetails' => "{$api_host}/api/user",
            'urlClubMemberships'      => "{$api_host}/api/user/communities/clubs",
            'urlClassYear'            => "{$api_host}/api/user/degrees/mit/classYear",
        );

        $options       = array_merge(
            $options,
            $default_options
        );
        $collaborators = array_merge(
            $collaborators,
            array(
                'optionProvider' => new League\OAuth2\Client\OptionProvider\HttpBasicAuthOptionProvider(),
            )
        );
        parent::__construct($options, $collaborators);
    }


    protected function getRequiredOptions()
    {
        return array_merge(
            parent::getRequiredOptions(),
            array(
                'production',
                'urlClassYear',
                'urlClubMemberships',
            )
        );
    }

    /**
     * Returns the JWT.
     *
     * @param $token AccessToken
     *
     * @return array
     */
    public function decodeJwt(AccessToken $token)
    {
        $payload       = explode('.', $token)[1];
        $base64_decode = base64_decode($payload);
        return json_decode($base64_decode, true);
    }


    /**
     * Returns the username of the user.
     *
     * The username is read directly from the access token, which should be a JWT.
     *
     * @param $token AccessToken
     *
     * @return string
     */
    public function getUsername(AccessToken $token)
    {
        return $this->decodeJwt($token)['user_name'];
    }

    public function getExpiration(AccessToken $token)
    {
        return $this->decodeJwt($token)['exp'];
    }


    /**
     * Requests and returns details of the user.
     *
     * Data includes username, name, email, affiliation, and club memberships.
     *
     * When converted to an array, the returned value will have the following keys:
     *  - username
     *  - advanceId
     *  - encryptedAdvanceId
     *  - encompassId
     *  - affiliation
     *  - affiliationDesc
     *  - firstName
     *  - lastName
     *  - emailAddress
     *  - blacklisted
     *  - deceased
     *  - graduating
     *  - clubs (array of arrays, with the keys below)
     *    - communityId
     *    - club
     *    - clubDesc
     *    - membership
     *    - membershipDesc
     *    - url
     *
     * @param AccessToken $token
     *
     * @return ResourceOwnerInterface
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function getResourceOwner(AccessToken $token)
    {
        $username_key = 'username';
        $username     = $this->getUsername($token);

        // Keys: advanceId, encryptedAdvanceId, encompassId, affiliation, affiliationDesc,
        // firstName, lastName, emailAddress, blacklisted, deceased, graduating
        $user_details = parent::getResourceOwner($token)->toArray();

        // Top-level key: clubs
        // Nested keys: communityId, club, clubDesc, membership, membershipDesc, url
        $request     = $this->getAuthenticatedRequest(
            self::METHOD_GET,
            $this->urlClubMemberships,
            $token
        );
        $memberships = $this->getParsedResponse($request);

        // Top-level key: classYear
        $request     = $this->getAuthenticatedRequest(
            self::METHOD_GET,
            $this->urlClassYear,
            $token
        );
        $classYear = $this->getParsedResponse($request);

        return new GenericResourceOwner(
            array_merge(
                $user_details,
                $memberships,
                $classYear,
                array($username_key => $username)
            ),
            $username_key
        );
    }
}
