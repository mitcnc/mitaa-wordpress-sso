=== MIT Alumni Association SSO ===
Contributors: ccb621
Donate link: https://giving.mit.edu
Tags: login, sso, MIT
Requires at least: 6.1.1
Tested up to: 6.1.1
Requires PHP: 8.0
Stable tag: trunk
License: MIT

Enable users to login with SSO via MIT Alumni Association.

== Description ==

This plugin enables WordPress installations to use the Infinite Connection
service for single sign-on. Usage requires OAuth 2.0 credentials, issued by
the Alumni Association.

== Installation ==

1. Coordinate with MITAA to get OAuth credentials. Your redirect_uri will follow the scheme: https://<hostname>/wp-admin/admin-ajax.php?action=mitaa-sso-authorize.
2. Install the plugin via Composer. See https://www.smashingmagazine.com/2019/03/composer-wordpress/
   to learn more about installing WordPress plugins via Composer.
3. Activate the plugin as you would any other plugin.
4. Configure the OAuth 2.0 credentials at `Settings > MITAA SSO`.
5. Create a login page (ideally at `/login`) and select the "MITAA SSO Login" template.

When users navigate to the login page they will go through the SSO flow, and be
redirected back to your site after successful login.

== Error handling ==

Errors will be logged to Sentry if the WordPress Sentry plugin [1] installed.

[1] https://wordpress.org/plugins/wp-sentry-integration/

== Development ==

The plugin can be used for local development with a few tweaks.

1. Set `'redirectUri' => 'https://alpha.mitcnc.org/wp-admin/admin-ajax.php?action=' . self::REDIRECT_ACTION_NAME,` in
    the `get_provider` method, replacing the value there now. This will ensure the authentication service does not reject
    requests due to an invalid redirect URL host.
2. Configure the plugin to use the development authentication service (e.g., not production).
3. Update the hosts file on your system to point `alpha.mitcnc.org` to `127.0.0.1` to ensure requests never go to the
   actual development server.
4. When redirected after login, change the hostname in the browser from 'https://alpha.mitcnc.org' to
   'http://localhost:<port>', where `<port>` is the port number where WordPress is being served.

Need to debug an error? Add the block of code below to `mitaa-wordpress-sso.php` to display errors in the browser.

    ini_set('display_errors', 1);
    error_reporting(E_ALL);


== Actions ==

Want to refresh user data? Use the `mitaa_sso_refresh_user_details` action. If the user has a cached
access token, data will be retrieved from the MITAA servers and returned. The user's metadata will
also be updated.

    do_action( 'mitaa_sso_refresh_user_details', $user->ID );
