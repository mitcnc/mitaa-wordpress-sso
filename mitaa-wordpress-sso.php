<?php
/**
 * Plugin Name: MIT Alumni Association SSO
 * Plugin URI: https://gitlab.com/mitcnc/mitaa-wordpress-sso
 * Description: Enable users to login with SSO via MIT Alumni Association.
 * Version: 1.0.0
 * Requires at least: 4.9
 * Requires PHP: 7.2
 * Text Domain: mitaa-sso
 * Author: Clinton Blackburn '08
 * Author URI: https://www.mitcnc.org/
 * License: MIT
 **/

use League\OAuth2\Client\Token\AccessToken;


defined('ABSPATH') || die('No script kiddies please!');

if (is_readable(__DIR__ . '/vendor/autoload.php')) {
    include __DIR__ . '/vendor/autoload.php';
}

require_once 'provider.php';

class MitaaSso
{
    /**
     * Static property to hold our singleton instance
     */
    public static $instance = null;

    protected const OPTION_NAME = 'mitaa_sso';
    protected const OPTION_GROUP = self::OPTION_NAME;
    protected const OPTIONS_PAGE = 'mitaa-sso';
    protected const STATE_TRANSIENT_PREFIX = 'mitaa-sso-state--';
    protected const TOKEN_TRANSIENT_PREFIX = 'mitaa-sso-token--';
    protected const STATE_EXPIRES_IN = 180;
    protected const REDIRECT_ACTION_NAME = 'mitaa-sso-authorize';
    protected const REDIRECT_COOKIE_NAME = 'mitaa-sso-redirect';
    protected const USER_META_PREFIX = 'mitaa-';
    protected const USER_META_KEY_IS_MEMBER = 'is_club_member';
    protected const USER_META_KEY_MEMBER_DATA = 'member_data';
    protected const USER_META_KEY_MEMBERSHIPS = 'club_memberships';
    protected const USER_META_KEY_ADVANCE_ID = 'advance_id';
    protected const LOGIN_PAGE_TEMPLATE_SLUG = 'template-login.php';


    private $provider = null;

    private function __construct()
    {
        add_action('admin_menu', array($this, 'initialize_admin_menu'));
        add_action('admin_init', array($this, 'initialize_settings'));

        // Register the callbacks that finish the authorization flow when the user is
        // redirected back from the authentication provider.
        add_action('wp_ajax_' . self::REDIRECT_ACTION_NAME, array($this, 'authorize_user'));
        add_action('wp_ajax_nopriv_' . self::REDIRECT_ACTION_NAME, array($this, 'authorize_user'));

        // Add a custom page template to simplify login
        add_filter('theme_page_templates', array($this, 'install_login_template'));
        add_filter('template_include', array($this, 'load_login_template'));

        // Add a custom column to the users list
        add_filter('manage_users_columns', array($this, 'add_users_columns'));
        add_filter('manage_users_custom_column', array($this, 'render_users_columns_data'), 10, 3);

        // Add MITAA data when displaying the user profile
        add_action('edit_user_profile', array($this, 'render_user_profile'));
        add_action('show_user_profile', array($this, 'render_user_profile'));

        // Add login button to login form, and set the redirect cookie when loading the form
        add_action('login_form_login', array($this, 'set_redirect_cookie'));
        add_filter('login_message', array($this, 'render_login_button'));

        // Add settings link on plugins page
        add_action('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'render_action_links'));

        // Ensure session cookies expire when the access token expires
        add_filter('auth_cookie_expiration', array($this, 'set_cookie_expiration'), 10, 3);

        // Allow external code (e.g., themes) to refresh user data
        add_action('mitaa_sso_refresh_user_details', array($this, 'refresh_user_details'), 10, 1);
    }

    public static function get_instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Add a submenu to the Settings menu.
     */
    public function initialize_admin_menu()
    {
        add_options_page(
            __('Configure MITAA SSO', 'mitaa-sso'),
            __('MITAA SSO', 'mitaa-sso'),
            'manage_options',
            self::OPTIONS_PAGE,
            array($this, 'render_options_page')
        );
    }

    /**
     * Gets the option, including defaults if a field is not set.
     *
     * This is especially important for the production setting, which
     * is not set at all if the value was set to false on the settings form.
     *
     * @return array
     */
    private function get_option()
    {
        $option_values = get_option(self::OPTION_NAME);
        $default_values = array(
            'client_id' => '',
            'client_secret' => '',
            'production' => 0,
            'display_btn_on_login_form' => 0,
            'disable_state_validation' => 0,
        );
        return shortcode_atts($default_values, $option_values);
    }

    /**
     * Initialize the custom settings page.
     */
    public function initialize_settings()
    {
        register_setting(
            self::OPTION_GROUP,
            self::OPTION_NAME,
            array(
                'sanitize_callback' => array($this, 'validate_options'),
            )
        );
        add_settings_section('main', '', array($this, 'render_section'), self::OPTIONS_PAGE);

        $data = $this->get_option();

        $fields = array(
            array(
                'id' => 'client_id',
                'label' => __('Client ID', 'mitaa-sso'),
                'type' => 'text',

            ),
            array(
                'id' => 'client_secret',
                'label' => __('Client Secret', 'mitaa-sso'),
                'type' => 'password',
            ),
            array(
                'id' => 'production',
                'label' => __('Use production server?', 'mitaa-sso'),
                'type' => 'checkbox',
            ),
            array(
                'id' => 'display_btn_on_login_form',
                'label' => __('Display button on login form?', 'mitaa-sso'),
                'type' => 'checkbox',
            ),
            array(
                'id' => 'disable_state_validation',
                'label' => __('Disable validation of the state parameter when redirecting from the auth provider', 'mitaa-sso'),
                'type' => 'checkbox',
            ),
        );
        foreach ($fields as $field) {
            $field_id = $field['id'];
            $args = array_merge(
                $field,
                array(
                    'label_for' => $field_id,
                    'value' => $data[$field_id],
                )
            );
            add_settings_field(
                $field_id,
                $field['label'],
                array($this, 'render_settings_field'),
                self::OPTIONS_PAGE,
                'main',
                $args
            );
        }
    }

    public function validate_options($input)
    {
        $valid = true;

        if (empty($input['client_id'])) {
            $valid = false;
            add_settings_error(
                'client_id',
                'client_id_required',
                __('A Client ID is required to configure MITAA SSO. No changes were saved.', 'mitaa-sso')
            );
        }

        if (empty($input['client_secret'])) {
            $valid = false;
            add_settings_error(
                'client_secret',
                'client_secret_required',
                __('A Client secret is required to configure MITAA SSO. No changes were saved.', 'mitaa-sso')
            );
        }

        // We return the original data if the passed in data is invalid
        // to ensure we don't overwrite good data with bad.
        return $valid ? $input : $this->get_option();
    }

    public function render_section($arguments): void
    {
        // TODO Why is this empty?
    }

    /**
     * Render an individual field on the settings page.
     *
     * @param $arguments
     */
    public function render_settings_field($arguments)
    {
        $field_id = esc_attr($arguments['id']);
        $value = $arguments['value'];
        $type = esc_attr($arguments['type']);

        // phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
        //  Value is the only user-provided field, and it is sanitized above.
        switch ($arguments['type']) {
            case 'password':
            case 'text':
                printf(
                    '<input name="%1$s[%2$s]" id="%2$s" type="%3$s" value="%4$s" required />',
                    esc_attr(self::OPTION_NAME),
                    $field_id,
                    $type,
                    esc_attr($value)
                );
                break;
            case 'checkbox':
                printf(
                    '<input name="%1$s[%2$s]" id="%2$s" type="%3$s" value="1" %4$s />',
                    esc_attr(self::OPTION_NAME),
                    $field_id,
                    $type,
                    checked(1, $value, false)
                );
                break;
            default:
                die("Encountered unexpected field type: {$arguments['type']}");
        }
        // phpcs:enable
    }

    /**
     * Render the options page where users configure the OAuth 2.0 client.
     */
    public function render_options_page()
    {
        ?>
        <div class="wrap">
            <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
            <form action="options.php" method="post" autocomplete="off">
                <?php
                settings_fields(self::OPTION_GROUP);
                do_settings_sections(self::OPTIONS_PAGE);
                submit_button();
                ?>
            </form>
            <a href="<?php echo esc_url($this->get_authorization_url()); ?>" target="_blank">Test</a> (Open in an
            incognito
            window)
        </div>
        <?php
    }

    private function get_provider(): MITAlumniAssociation
    {
        if (!$this->provider) {
            $options = $this->get_option();
            $this->provider = new MITAlumniAssociation(
                array(
                    'clientId' => $options['client_id'],
                    'clientSecret' => $options['client_secret'],
                    'redirectUri' => admin_url('admin-ajax.php?action=' . self::REDIRECT_ACTION_NAME),
                    'production' => boolval($options['production']),
                )
            );
        }

        return $this->provider;
    }

    /**
     * Login the user by redirecting to MITAA.
     */
    public function login()
    {
        $this->set_redirect_cookie();
        $authorizationUrl = $this->get_authorization_url();

        // Redirect the user
        header('Location: ' . $authorizationUrl);
        exit;
    }

    /**
     * Validates the given state value against all cached values.
     *
     * @param $state string to be validated
     *
     * @return boolean
     */
    public function validate_state(string $state): bool
    {
        return !!get_transient(self::STATE_TRANSIENT_PREFIX . $state);
    }

    private function log_sentry_exception($e, ?string $contextKey = null, ?array $contextValue = null)
    {
        if (function_exists('wp_sentry_safe')) {
            wp_sentry_safe(
                function (\Sentry\State\HubInterface $client) use ($e, $contextKey, $contextValue): void {
                    $client->withScope(
                        function (\Sentry\State\Scope $scope) use ($client, $e, $contextKey, $contextValue) {
                            if ($contextKey && $contextValue) {
                                $scope->setContext($contextKey, $contextValue);
                            }
                            $client->captureException($e);
                        }
                    );
                }
            );
        }
    }

    private function log_sentry_message($msg, ?string $contextKey = null, ?array $contextValue = null)
    {
        if (function_exists('wp_sentry_safe')) {
            wp_sentry_safe(
                function (\Sentry\State\HubInterface $client) use ($msg, $contextKey, $contextValue): void {
                    $client->withScope(
                        function (\Sentry\State\Scope $scope) use ($client, $msg, $contextKey, $contextValue) {
                            if ($contextKey && $contextValue) {
                                $scope->setContext($contextKey, $contextValue);
                            }
                            $client->captureMessage($msg);
                        }
                    );
                }
            );
        }
    }

    public function authorize_user(): void
    {
        $options = $this->get_option();

        // Verify we received the correct state. A failure here may indicate a malicious actor.
        if (
            !boolval($options['disable_state_validation']) &&
            (
                empty($_GET['state']) ||
                !$this->validate_state(sanitize_text_field(wp_unslash($_GET['state'])))
            )
        ) {
            $this->log_sentry_message('state parameter is invalid');
            wp_die('The authentication server response has an invalid `state` parameter. Please try again.');
        }

        // We MUST have a code to continue. If we don't have one, something
        // broke with the provider.
        if (empty($_GET['code'])) {
            $this->log_sentry_message('code parameter is invalid');
            wp_die('The authentication server response is missing the `code` parameter. Please try again.');
        }

        $provider = $this->get_provider();

        // Retrieve the access token so we can use the API to get user data
        try {
            $access_token = $provider->getAccessToken(
                'authorization_code',
                array(
                    'code' => sanitize_text_field(wp_unslash($_GET['code'])),
                )
            );
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            $this->log_sentry_exception($e);
            wp_die(esc_html($e->getMessage()));
        }

        $user_details = $this->get_user_details_from_provider($access_token);
        $user = $this->update_or_create_user($user_details);

        $this->cache_access_token($user->ID, $access_token);

        $this->login_and_redirect_user($user);
    }

    /**
     * Get the first user matching to the query, or return false.
     *
     * @param $query array
     *
     * @return ?WP_User WP_User object on success, false on failure.
     */
    private function get_first_user(array $query): ?WP_User
    {
        $users = get_users($query);
        return $users ? $users[0] : null;
    }

    /**
     * Try to find a user with the given details.
     *
     * We first search by email. If we cannot find a user with a matching email address,
     * we fallback to the MITAA Advance ID.
     *
     * @param $user_details array
     *
     * @return ?WP_User WP_User object on success, false on failure.
     */
    private function find_user(array $user_details): ?WP_User
    {
        $user = get_user_by('email', $user_details['emailAddress']);
        $user = $user ?? get_user_by('login', $user_details['username']);

        if (empty($user)) {
            $user = $this->get_first_user(
                array(
                    'meta_query' => array(
                        'relation' => 'OR',
                        array(
                            'key' => self::USER_META_PREFIX . self::USER_META_KEY_ADVANCE_ID,
                            'value' => $user_details['advanceId'],
                        ),
                        // NOTE: This is needed for the MITCNC website. Ideally,
                        // we should only rely on prefixed fields.
                        array(
                            'key' => 'advance_id',
                            'value' => $user_details['advanceId'],
                        ),
                    )
                )
            );
        }

        return empty($user) ? null : $user;
    }

    private function cache_access_token(int $user_id, AccessToken $access_token): void
    {
        set_transient(self::TOKEN_TRANSIENT_PREFIX . $user_id, $access_token, $access_token->getExpires());
    }

    /**
     * Returns the access token that has been cached for the user.
     *
     * @param int $user_id
     * @return ?AccessToken
     */
    private function get_cached_access_token(int $user_id): ?AccessToken
    {
        $token = get_transient(self::TOKEN_TRANSIENT_PREFIX . $user_id);
        return empty($token) ? null : $token;
    }

    /**
     * Refreshes the user's data (e.g., memberships) from MITAA.
     *
     * @param $user_id int
     *
     * @return array | null
     */
    public function refresh_user_details(int $user_id): ?array
    {
        $user_details = null;
        $access_token = $this->get_cached_access_token($user_id);

        if (!empty($access_token)) {
            $user_details = $this->get_user_details_from_provider($access_token);
            $this->set_user_club_memberships($user_id, $user_details);
        }

        return $user_details;
    }

    /**
     * Update the user's club memberships in the DB.
     *
     * @param $user_id int
     * @param $user_details array
     */
    private function set_user_club_memberships(int $user_id, array $user_details)
    {
        $is_club_member = !empty($user_details['clubs']);
        update_user_meta($user_id, self::USER_META_PREFIX . self::USER_META_KEY_IS_MEMBER, $is_club_member);
        update_user_meta($user_id, self::USER_META_PREFIX . self::USER_META_KEY_MEMBERSHIPS, $user_details['clubs']);
        update_user_meta($user_id, self::USER_META_PREFIX . self::USER_META_KEY_MEMBER_DATA, $user_details);
    }

    private function update_or_create_user($user_details): WP_User
    {
        $user = $this->find_user($user_details);

        // Create the user if we could not find one
        if (empty($user)) {
            $user_data = array(
                'user_login' => $user_details['username'],
                'user_pass' => wp_generate_password(32, true, true),
                'user_email' => $user_details['emailAddress'],
                'first_name' => isset($user_details['firstName']) ? $user_details['firstName'] : '',
                'last_name' => isset($user_details['lastName']) ? $user_details['lastName'] : '',
            );
            $uid = wp_insert_user($user_data);

            if (is_wp_error($uid)) {
                unset($user_data['user_pass']);
                $this->log_sentry_message('User creation failed!', 'user_data', $user_data);
                throw new Exception($uid->get_error_message());
            }

            update_user_meta($uid, self::USER_META_PREFIX . self::USER_META_KEY_ADVANCE_ID, $user_details['advanceId']);

            $user = get_user_by('id', $uid);
        }

        $this->set_user_club_memberships($user->ID, $user_details);

        return $user;
    }

    /**
     * Set a cookie so that we can redirect the user back to where they started after
     * a successful login.
     *
     * No cookie is set if the user is starting from the logout page.
     */
    public function set_redirect_cookie(): void
    {
        $on_wp_login_page = 'wp-login.php' === $GLOBALS['pagenow'];
        $on_custom_login_page = self::LOGIN_PAGE_TEMPLATE_SLUG === get_page_template_slug();

        // Never let the redirect be set to the logout action/page
        if (
            $on_wp_login_page &&
            isset($_GET['action']) &&
            'logout' === $_GET['action']
        ) {
            return;
        }

        $redirect_expiry = current_time('timestamp') + DAY_IN_SECONDS;
        $redirect_url = null;

        if ($on_wp_login_page) {
            // Redirect to the admin dashboard by default, if the user navigated
            // to the standard login page directly (as opposed to being redirected).
            $redirect_url = admin_url();
        } elseif (($on_wp_login_page || $on_custom_login_page) && isset($_REQUEST['redirect_to'])) {
            // Allow the query string to override the redirect URL
            $redirect_url = esc_url_raw(wp_unslash($_REQUEST['redirect_to']));
        } else {
            // As a last resort, send the user back to the page where they clicked the login button.
            $referer = wp_get_referer();

            if ($referer) {
                $redirect_url = $referer;
            }
        }

        // If we still don't have a redirect URL, bail out.
        if (!$redirect_url) {
            return;
        }

        setcookie(
            self::REDIRECT_COOKIE_NAME,
            $redirect_url,
            $redirect_expiry,
            COOKIEPATH,
            COOKIE_DOMAIN,
            is_ssl()
        );
    }

    /**
     * Returns a URL the user can access to initiate authorization/login.
     *
     * @return string
     */
    public function get_authorization_url(): string
    {
        $provider = $this->get_provider();
        $authorizationUrl = $provider->getAuthorizationUrl();

        // Cache the state so that we can validate it after the user logs in
        // at the provider. This will help protect against CSRF attacks.
        $state = $provider->getState();
        set_transient(self::STATE_TRANSIENT_PREFIX . $state, $state, self::STATE_EXPIRES_IN);

        return $authorizationUrl;
    }

    public function install_login_template($page_templates)
    {
        $page_templates[self::LOGIN_PAGE_TEMPLATE_SLUG] = __('MITAA SSO Login', 'mitaa-sso');

        return $page_templates;
    }

    public function load_login_template($template)
    {
        $template_name = self::LOGIN_PAGE_TEMPLATE_SLUG;

        if (get_page_template_slug() === $template_name) {
            $template = plugin_dir_path(__FILE__) . $template_name;
        }

        return $template;
    }

    public function add_users_columns($columns)
    {
        $columns['is_mitaa_club_member'] = __('MITAA Member?', 'mitaa-sso');
        return $columns;
    }

    public function render_users_columns_data($output, $column_name, $user_id)
    {
        if ('is_mitaa_club_member' === $column_name) {
            return get_user_meta($user_id, self::USER_META_PREFIX . self::USER_META_KEY_IS_MEMBER, true) ?
                __('Yes', 'mitaa-sso') :
                __('No', 'mitaa-sso');
        }

        return $output;
    }

    public function render_user_profile($user)
    {
        $member_data = get_user_meta($user->ID, self::USER_META_PREFIX . self::USER_META_KEY_MEMBER_DATA, true);
        $memberships = get_user_meta($user->ID, self::USER_META_PREFIX . self::USER_META_KEY_MEMBERSHIPS, true);
        ?>
        <h2><?php esc_html_e('MITAA Data', 'mitaa-sso'); ?></h2>

        <?php
        if (empty($memberships)) {
            ?>
            <em><?php esc_html_e('No membership data available', 'mitaa-sso'); ?></em>
            <?php
        } else {
            ?>
            <p>
                <strong>Class year</strong>&nbsp;<?php echo esc_html($member_data['classYear'] ?? '(None)'); ?>
            </p>
            <table class="widefat fixed">
                <thead>
                <tr>
                    <th><?php esc_html_e('Club', 'mitaa-sso'); ?></th>
                    <th><?php esc_html_e('Membership Level', 'mitaa-sso'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($memberships as $membership) {
                    ?>
                    <tr class="alternate">
                        <td><?php echo esc_html($membership['clubDesc']); ?></td>
                        <td><?php echo esc_html($membership['membershipDesc']); ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>

            <p>
            <h3>Raw data</h3>
            <pre><?php echo esc_html(print_r($member_data, true)); ?></pre>
            </p>
            <?php
        }
    }

    public function render_login_button($message)
    {
        $options = $this->get_option();

        if (boolval($options['display_btn_on_login_form'])) {
            $url = $this->get_authorization_url();

            ob_start();
            ?>
            <div class="openid-connect-login-button" style="margin: 1em 0; text-align: center;">
                <a class="button button-large" href="<?php echo esc_url($url); ?>">
                    <?php esc_html_e('Login via Infinite Connection', 'mitaa-sso'); ?>
                </a>
            </div>
            <?php

            // Append the login button to the existing message atop the form
            $message .= ob_get_clean();
        }

        return $message;
    }

    public function render_action_links($links)
    {
        $url = add_query_arg(
            'page',
            self::OPTIONS_PAGE,
            get_admin_url() . 'options-general.php'
        );

        ob_start();
        ?>
        <a href="<?php echo esc_url($url); ?>">
            <?php esc_html_e('Settings', 'mitaa-sso'); ?>
        </a>
        <?php

        $links = array_merge(
            array(ob_get_clean()),
            $links
        );

        return $links;
    }

    /**
     * Retrieves user details from MITAA.
     *
     * @param $access_token AccessToken
     * @return array
     */
    private function get_user_details_from_provider($access_token)
    {
        try {
            $user_details = $this->get_provider()->getResourceOwner($access_token)->toArray();
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            $this->log_sentry_exception($e);
            exit(esc_html($e->getMessage()));
        }

        return $user_details;
    }

    /**
     * Logs the user into WordPress and redirects to the URL
     * set in the cookie prior to login.
     *
     * @param $user WP_User
     */
    private function login_and_redirect_user($user)
    {
        // Authenticate the user with WordPress
        wp_set_auth_cookie($user->ID, true);
        do_action('wp_login', $user->user_login, $user);

        $redirect_url = isset($_COOKIE[self::REDIRECT_COOKIE_NAME]) ?
            esc_url_raw(wp_unslash($_COOKIE[self::REDIRECT_COOKIE_NAME])) :
            false;
        if ($redirect_url) {
            // Unset the cookie to avoid potential errors
            unset($_COOKIE[self::REDIRECT_COOKIE_NAME]);

            wp_redirect($redirect_url);
        } else {
            wp_safe_redirect(home_url());
        }

        exit;
    }

    /**
     * Set the cookie to expire when the access token expires.
     *
     * @param $length int
     * @param $user_id int
     * @param $remember bool
     *
     * @return int
     */
    public function set_cookie_expiration($length, $user_id, $remember)
    {
        $access_token = $this->get_cached_access_token($user_id);

        if ($access_token) {
            $expiration = $this->get_provider()->getExpiration($access_token);

            // Token expiration time must be greater than zero; otherwise, the user
            // will never be able to login if we haven't removed an expired token from the cache.
            $token_expiration = $expiration - time();
            if ($token_expiration > 0) {
                return $token_expiration;
            } else {
                delete_expired_transients();
            }
        }

        return $length;
    }
}

MitaaSso::get_instance();
