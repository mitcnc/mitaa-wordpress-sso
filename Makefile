.DEFAULT_GOAL := quality

.PHONY: requirements quality

# Generates a help message. Borrowed from https://github.com/pydanny/cookiecutter-djangopackage.
help: ## Display this help message
	@echo "Please use \`make <target>\` where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'

requirements: ## Install requirements for local development
	composer install

quality: ## Run code quality checks
	./vendor/bin/phpcbf
	./vendor/bin/phpcs

docker.pull: ## Pull the Docker containers
	docker-compose pull

docker.down: ## Stop the Docker containers
	docker-compose -f docker-compose.yml -f docker-compose.yml down

docker.restart: ## Restart the Docker containers
	docker-compose -f docker-compose.yml -f docker-compose.yml restart

docker.shell: ## Open a shell into the WordPress Docker container
	docker-compose -f docker-compose.yml -f docker-compose.yml exec wordpress /bin/bash

docker.up: ## Start the Docker containers
	docker-compose -f docker-compose.yml -f docker-compose.yml up -d
