<?php
/*
Template Name: MITAA SSO Login
*/

if (is_user_logged_in()) {
    $REDIRECT_QUERY_ARG = 'redirect_to';
    $redirect_url = false;

    // Allow the query string to override the redirect URL
    if (isset($_REQUEST[$REDIRECT_QUERY_ARG])) {
        $redirect_url = esc_url_raw(wp_unslash($_REQUEST[$REDIRECT_QUERY_ARG]));
    }

    if ($redirect_url) {
        wp_redirect($redirect_url);
    } else {
        wp_safe_redirect(home_url());
    }
} else {
    $sso_wrapper = MitaaSso::get_instance();
    $sso_wrapper->login();
}

exit;
